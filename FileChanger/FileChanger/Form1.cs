﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FileChanger
{
    public partial class Form1 : Form
    {
        String[] listOldNames;
        String[] listNewNames;
        FileProcess fileProcess;

        public Form1()
        {
            InitializeComponent();
        }

        private void BtnSearchPath_Click(object sender, EventArgs e)
        {
            txtOriginPath.Text = getSelectedPathByUser();
        }

        private void BtnSelectPath_Click(object sender, EventArgs e)
        {
            txtDestinyPath.Text = getSelectedPathByUser();
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            string errorMessage = this.validateForm();

            if (String.IsNullOrEmpty(errorMessage))
            {
                lstComplete.Items.Clear();
                lstNotFound.Items.Clear();
                lstPartials.Items.Clear();

                for (int i = 0; i < listOldNames.Length; i++)
                {
                    if (!String.IsNullOrEmpty(listOldNames[i]) && !String.IsNullOrEmpty(listNewNames[i]))
                    {
                        string name = listOldNames[i];
                        fileProcess = new FileProcess(
                                txtOriginPath.Text,
                                txtDestinyPath.Text,
                                name,
                                listNewNames[i],
                                chkSubFolders.Checked,
                                chkPartials.Checked
                            );
                        if (fileProcess.TotalFileFound == 0)
                        {
                            lstNotFound.Items.Add(name);
                        }
                        else
                        {
                            lstComplete.Items.Add(name);
                            if (chkPartials.Checked)
                            {
                                foreach(string fileEntrie in fileProcess.FileEntries)
                                {
                                    lstPartials.Items.Add(Path.GetFileName(fileEntrie));
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show(errorMessage);
            }
        }

        private String getSelectedPathByUser()
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.RootFolder = Environment.SpecialFolder.Desktop;
            folderBrowserDialog.Description = "Seleccionar carpeta";
            folderBrowserDialog.ShowNewFolderButton = false;

            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                return folderBrowserDialog.SelectedPath;
            }

            return null;
        }

        private String validateForm()
        {
            if (String.IsNullOrEmpty(txtOriginPath.Text))
            {
                return "Favor de seleccionar la carpeta de origen";
            }

            if (String.IsNullOrEmpty(txtDestinyPath.Text))
            {
                return "Favor de seleccionar la carpeta de destino";
            }

            if (String.IsNullOrEmpty(txtNameFile.Text))
            {
                return "Favor de ingresar el nuevo nombre de los archivos";
            }

            if (String.IsNullOrEmpty(txtListNames.Text))
            {
                return "Favor de ingresar el nombre del archivo(s) a buscar";
            }

            listOldNames = txtListNames.Text.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            listNewNames = txtNameFile.Text.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            if (listOldNames.Length != listNewNames.Length)
            {
                return "Las listas de nombre no coinciden";
            }

            return null;
        }

        private void BtnLimpiar_Click(object sender, EventArgs e)
        {
            txtListNames.Clear();
            txtNameFile.Clear();
        }
    }
}
