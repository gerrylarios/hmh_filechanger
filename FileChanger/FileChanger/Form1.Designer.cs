﻿namespace FileChanger
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNameFile = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chkPartials = new System.Windows.Forms.CheckBox();
            this.chkSubFolders = new System.Windows.Forms.CheckBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnDestiny = new System.Windows.Forms.Label();
            this.btnOrigin = new System.Windows.Forms.Label();
            this.txtDestinyPath = new System.Windows.Forms.TextBox();
            this.btnSelectPath = new System.Windows.Forms.Button();
            this.txtOriginPath = new System.Windows.Forms.TextBox();
            this.btnSearchPath = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtListNames = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lstNotFound = new System.Windows.Forms.ListView();
            this.lstPartials = new System.Windows.Forms.ListView();
            this.lstComplete = new System.Windows.Forms.ListView();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnLimpiar);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtNameFile);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.btnDestiny);
            this.groupBox1.Controls.Add(this.btnOrigin);
            this.groupBox1.Controls.Add(this.txtDestinyPath);
            this.groupBox1.Controls.Add(this.btnSelectPath);
            this.groupBox1.Controls.Add(this.txtOriginPath);
            this.groupBox1.Controls.Add(this.btnSearchPath);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtListNames);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(776, 274);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Archivos";
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(10, 245);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(216, 23);
            this.btnLimpiar.TabIndex = 17;
            this.btnLimpiar.Text = "Limpiar Formulario";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.BtnLimpiar_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(276, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Nuevos nombres";
            // 
            // txtNameFile
            // 
            this.txtNameFile.Location = new System.Drawing.Point(279, 54);
            this.txtNameFile.Multiline = true;
            this.txtNameFile.Name = "txtNameFile";
            this.txtNameFile.Size = new System.Drawing.Size(220, 182);
            this.txtNameFile.TabIndex = 15;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chkPartials);
            this.groupBox3.Controls.Add(this.chkSubFolders);
            this.groupBox3.Controls.Add(this.btnStart);
            this.groupBox3.Location = new System.Drawing.Point(539, 146);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(220, 119);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Filtros";
            // 
            // chkPartials
            // 
            this.chkPartials.AutoSize = true;
            this.chkPartials.Checked = true;
            this.chkPartials.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPartials.Location = new System.Drawing.Point(6, 53);
            this.chkPartials.Name = "chkPartials";
            this.chkPartials.Size = new System.Drawing.Size(154, 17);
            this.chkPartials.TabIndex = 4;
            this.chkPartials.Text = "Coincidir completo y parcial";
            this.chkPartials.UseVisualStyleBackColor = true;
            // 
            // chkSubFolders
            // 
            this.chkSubFolders.AutoSize = true;
            this.chkSubFolders.Checked = true;
            this.chkSubFolders.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSubFolders.Location = new System.Drawing.Point(6, 30);
            this.chkSubFolders.Name = "chkSubFolders";
            this.chkSubFolders.Size = new System.Drawing.Size(135, 17);
            this.chkSubFolders.TabIndex = 3;
            this.chkSubFolders.Text = "Buscar en subcarpetas";
            this.chkSubFolders.UseVisualStyleBackColor = true;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(6, 90);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 5;
            this.btnStart.Text = "Empezar";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // btnDestiny
            // 
            this.btnDestiny.AutoSize = true;
            this.btnDestiny.Location = new System.Drawing.Point(536, 91);
            this.btnDestiny.Name = "btnDestiny";
            this.btnDestiny.Size = new System.Drawing.Size(81, 13);
            this.btnDestiny.TabIndex = 12;
            this.btnDestiny.Text = "Carpeta destino";
            // 
            // btnOrigin
            // 
            this.btnOrigin.AutoSize = true;
            this.btnOrigin.Location = new System.Drawing.Point(536, 36);
            this.btnOrigin.Name = "btnOrigin";
            this.btnOrigin.Size = new System.Drawing.Size(76, 13);
            this.btnOrigin.TabIndex = 11;
            this.btnOrigin.Text = "Carpeta origen";
            // 
            // txtDestinyPath
            // 
            this.txtDestinyPath.Location = new System.Drawing.Point(539, 120);
            this.txtDestinyPath.Name = "txtDestinyPath";
            this.txtDestinyPath.ReadOnly = true;
            this.txtDestinyPath.Size = new System.Drawing.Size(220, 20);
            this.txtDestinyPath.TabIndex = 10;
            // 
            // btnSelectPath
            // 
            this.btnSelectPath.Location = new System.Drawing.Point(684, 91);
            this.btnSelectPath.Name = "btnSelectPath";
            this.btnSelectPath.Size = new System.Drawing.Size(75, 23);
            this.btnSelectPath.TabIndex = 2;
            this.btnSelectPath.Text = "Buscar";
            this.btnSelectPath.UseVisualStyleBackColor = true;
            this.btnSelectPath.Click += new System.EventHandler(this.BtnSelectPath_Click);
            // 
            // txtOriginPath
            // 
            this.txtOriginPath.Location = new System.Drawing.Point(539, 65);
            this.txtOriginPath.Name = "txtOriginPath";
            this.txtOriginPath.ReadOnly = true;
            this.txtOriginPath.Size = new System.Drawing.Size(220, 20);
            this.txtOriginPath.TabIndex = 8;
            // 
            // btnSearchPath
            // 
            this.btnSearchPath.Location = new System.Drawing.Point(684, 36);
            this.btnSearchPath.Name = "btnSearchPath";
            this.btnSearchPath.Size = new System.Drawing.Size(75, 23);
            this.btnSearchPath.TabIndex = 1;
            this.btnSearchPath.Text = "Buscar";
            this.btnSearchPath.UseVisualStyleBackColor = true;
            this.btnSearchPath.Click += new System.EventHandler(this.BtnSearchPath_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Lista de nombres";
            // 
            // txtListNames
            // 
            this.txtListNames.Location = new System.Drawing.Point(10, 56);
            this.txtListNames.Multiline = true;
            this.txtListNames.Name = "txtListNames";
            this.txtListNames.Size = new System.Drawing.Size(220, 180);
            this.txtListNames.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.lstNotFound);
            this.groupBox2.Controls.Add(this.lstPartials);
            this.groupBox2.Controls.Add(this.lstComplete);
            this.groupBox2.Location = new System.Drawing.Point(12, 292);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(776, 237);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Resultados";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(547, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "No encontrados";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(276, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Parciales";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Completados";
            // 
            // lstNotFound
            // 
            this.lstNotFound.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.lstNotFound.Location = new System.Drawing.Point(550, 49);
            this.lstNotFound.Name = "lstNotFound";
            this.lstNotFound.Size = new System.Drawing.Size(220, 182);
            this.lstNotFound.TabIndex = 8;
            this.lstNotFound.UseCompatibleStateImageBehavior = false;
            this.lstNotFound.View = System.Windows.Forms.View.List;
            // 
            // lstPartials
            // 
            this.lstPartials.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.lstPartials.Location = new System.Drawing.Point(279, 49);
            this.lstPartials.Name = "lstPartials";
            this.lstPartials.Size = new System.Drawing.Size(220, 182);
            this.lstPartials.TabIndex = 7;
            this.lstPartials.UseCompatibleStateImageBehavior = false;
            this.lstPartials.View = System.Windows.Forms.View.List;
            // 
            // lstComplete
            // 
            this.lstComplete.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.lstComplete.Location = new System.Drawing.Point(6, 49);
            this.lstComplete.Name = "lstComplete";
            this.lstComplete.Size = new System.Drawing.Size(220, 182);
            this.lstComplete.TabIndex = 6;
            this.lstComplete.UseCompatibleStateImageBehavior = false;
            this.lstComplete.View = System.Windows.Forms.View.List;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 541);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView lstNotFound;
        private System.Windows.Forms.ListView lstPartials;
        private System.Windows.Forms.ListView lstComplete;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtListNames;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label btnDestiny;
        private System.Windows.Forms.Label btnOrigin;
        private System.Windows.Forms.TextBox txtDestinyPath;
        private System.Windows.Forms.Button btnSelectPath;
        private System.Windows.Forms.TextBox txtOriginPath;
        private System.Windows.Forms.Button btnSearchPath;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox chkPartials;
        private System.Windows.Forms.CheckBox chkSubFolders;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNameFile;
        private System.Windows.Forms.Button btnLimpiar;
    }
}

