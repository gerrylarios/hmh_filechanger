﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileChanger
{
    class FileProcess
    {
        bool isSubFolderAllowed;
        bool isPartialAllowed;
        string originPath;
        string fileNewName;
        string fileOldName;
        string destinyPath;
        string [] fileEntries;
        int totalFileFound;

        public FileProcess (string originPath, string destinyPath, string fileOldName, string fileNewName, bool isSubFolderAllowed, bool isPartialAllowed)
        {
            this.originPath = originPath;
            this.destinyPath = destinyPath;

            this.fileOldName = fileOldName;
            this.fileNewName = fileNewName;

            this.isSubFolderAllowed = isSubFolderAllowed;
            this.isPartialAllowed = isPartialAllowed;

            this.init();
        }

        private void init ()
        {
            string pattern = fileOldName + ".*";

            if (isPartialAllowed)
            {
                pattern = fileOldName + "*";
            }

            if(isSubFolderAllowed)
            {
                this.fileEntries = Directory.GetFiles(originPath, pattern, SearchOption.AllDirectories);
                totalFileFound = fileEntries.Length;
            }
            else
            {
                this.fileEntries = Directory.GetFiles(originPath, pattern, SearchOption.TopDirectoryOnly);
                totalFileFound = fileEntries.Length;
            }

            copyFiles();
        }

        private void copyFiles()
        {
            int i = 0;
            foreach(string name in fileEntries)
            {
                string destFileName = destinyPath + "\\" + fileNewName + "_" + i + Path.GetExtension(name);
                File.Copy(name, destFileName, true);
                i++;
            }
        }

        public bool IsSubFolderAllowed { get => isSubFolderAllowed; set => isSubFolderAllowed = value; }
        public bool IsPartialAllowed { get => isPartialAllowed; set => isPartialAllowed = value; }
        public string OriginPath { get => originPath; set => originPath = value; }
        public string FileNewName { get => fileNewName; set => fileNewName = value; }
        public string FileOldName { get => fileOldName; set => fileOldName = value; }
        public string DestinyPath { get => destinyPath; set => destinyPath = value; }
        public string[] FileEntries { get => fileEntries; }
        public int TotalFileFound { get => totalFileFound; }
    }
}
